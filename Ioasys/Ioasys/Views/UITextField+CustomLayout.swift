//
//  UITextField+CustomLayout.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 28/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import UIKit

@IBDesignable extension UITextField {
    @IBInspectable var hasUnderlineBorder: Bool {
        set {
            let border = CALayer()
            let lineHeight: CGFloat = 1
            let lineWidth: CGFloat = UIScreen.main.bounds.width
            let borderRect = CGRect(x: 0, y: self.frame.size.height - lineHeight, width:  lineWidth, height: lineHeight)
            
            border.backgroundColor = UIColor(red: 56.0/255, green: 55.0/255, blue: 67.0/255, alpha: 1.0).cgColor
            border.frame = borderRect

            self.layer.masksToBounds = true
            
            self.layer.addSublayer(border)
        }
        get {
            return self.hasUnderlineBorder
        }
    }
    
    @IBInspectable var icon: UIImage {
        set {
            let iconView = UIImageView(frame: CGRect(x: 0, y: 2, width: 25, height: 25))
            iconView.image = newValue
            iconView.sizeToFit()
            let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            iconContainerView.addSubview(iconView)
            leftView = iconContainerView
            leftViewMode = .always
        }
        get {
            return self.icon
        }
    }
    
    @IBInspectable var placeholderColor: UIColor {
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                            attributes: [NSAttributedString.Key.foregroundColor: newValue])
        }
        get {
            return self.placeholderColor
        }
    }
}
