//
//  RoundedButtons.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 28/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import UIKit

@IBDesignable extension UIButton {
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
}
