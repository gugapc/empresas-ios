//
//  EnterpriseType.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 31/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import Foundation

struct EnterpriseType: Mappable {
    var id: Int?
    var enterprise_type_name: String?
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.enterprise_type_name = mapper.keyPath("enterprise_type_name")
    }
}
