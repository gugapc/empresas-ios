//
//  File.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 30/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import Foundation

struct User: Mappable {
    //    var enterprise
    var investor: Investor
    var success: Bool
    
    init(mapper: Mapper) {
        self.investor = mapper.keyPath("investor")
        self.success = mapper.keyPath("success")
    }
}
