//
//  EnterpriseData.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 31/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import Foundation

struct EnterpriseData: Mappable {
    var id: Int
    var email_enterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool?
    var enterprise_name: String?
    var photo: String?
    var description: String?
    var city: String?
    var country: String?
    var value: Int?
    var share_price: Double?
    var enterprise_type: EnterpriseType?

    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.email_enterprise = mapper.keyPath("email_enterprise")
        self.facebook = mapper.keyPath("facebook")
        self.twitter = mapper.keyPath("twitter")
        self.linkedin = mapper.keyPath("linkedin")
        self.phone = mapper.keyPath("phone")
        self.own_enterprise = mapper.keyPath("own_enterprise")
        self.enterprise_name = mapper.keyPath("enterprise_name")
        self.photo = mapper.keyPath("photo")
        self.description = mapper.keyPath("description")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.value = mapper.keyPath("value")
        self.share_price = mapper.keyPath("share_price")
        self.enterprise_type = mapper.keyPath("enterprise_type")
    }
}
