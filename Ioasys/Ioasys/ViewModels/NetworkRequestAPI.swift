//
//  AuthenticationAPI.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 30/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import Foundation

class NetworkRequestAPI: APIRequest {
    
    @discardableResult
    static func loginWith(username: String, password: String, callback: ResponseBlock<User>?) -> NetworkRequestAPI {
        
        let request = NetworkRequestAPI(method: .post, path: "/api/v1/users/auth/sign_in", parameters: ["email": username, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
                print("error")
            } else if let response = response as? [String: Any] {
                print(response)
                let user = User(dictionary: response)
                callback?(user, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
    
    @discardableResult
    static func searchWith(name: String, callback: ResponseBlock<Enterprises>?) -> NetworkRequestAPI {
        
        let request = NetworkRequestAPI(method: .get, path: "/api/v1/enterprises", parameters: nil, urlParameters: ["name": name], cacheOption: .both) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
                print("error")
            } else if let response = response as? [String: Any] {
                print(response)
                let enterprise = Enterprises(dictionary: response)
                callback?(enterprise, nil, cache)
            }
        }
        request.shouldSaveInCache = true
        request.makeRequest()
        
        return request
    }
}
