//
//  Enterprises.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 31/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import Foundation

struct Enterprises: Mappable {
    var enterprises: [EnterpriseData]
    
    init(mapper: Mapper) {
        self.enterprises = mapper.keyPath("enterprises")
    }
}
