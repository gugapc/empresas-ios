//
//  ViewController.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 28/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginLoader: UIActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromKeyboardNotifications()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func clickEnter(_ sender: Any) {
        logIn()
    }
    
    private func logIn() {
        // TODO: Handle empty field
        loginLoader.startAnimating()
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        NetworkRequestAPI.loginWith(username: email, password: password) { (user, error, cache) in
            self.loginLoader.stopAnimating()
            if user != nil {
                self.presentMainScreen()
            }
            // TODO: error handler
        }
    }
    
    private func presentMainScreen() {
        let enterprisesStoryboard: UIStoryboard = UIStoryboard(name: "Enterprises", bundle: nil)
        let enterprisesVC = enterprisesStoryboard.instantiateViewController(withIdentifier: "EnterprisesID") as! UINavigationController
        
        self.present(enterprisesVC, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - TextField delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            emailTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
            return true
        }
        textField.resignFirstResponder()
        logIn()
        return true;
    }
    
}

extension LoginViewController {
    // MARK: - Observe and respond to keyboard notifications
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let firstResponderTextfield = emailTextField.isFirstResponder ? emailTextField : passwordTextField

        let heightScreen = UIScreen.main.bounds.height
        let keyboardYPosition = heightScreen - self.getKeyboardHeight(notification: notification)
        let textfieldPositionWithContent: CGFloat = (firstResponderTextfield?.frame.origin.y ?? 0) + (firstResponderTextfield?.frame.height ?? 0)
        
        
        if keyboardYPosition < textfieldPositionWithContent {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= (textfieldPositionWithContent - keyboardYPosition)
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.view.frame.origin.y = 0
    }
    
    func getKeyboardHeight(notification: Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardFrameEnd: NSValue = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        
        return keyboardFrameEnd.cgRectValue.height
    }
}
