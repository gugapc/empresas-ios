//
//  EnterpriseDetailsViewController.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 31/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import UIKit

class EnterpriseDetailsViewController: UIViewController {
    @IBOutlet weak var enterpriseImage: UIImageView!
    @IBOutlet weak var imageLoader: UIActivityIndicatorView!
    @IBOutlet weak var enterpriseDescription: UITextView!

    var enterpriseData: EnterpriseData? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.reloadInputViews()
        configureNavigationItem()
        
        if let description = enterpriseData?.description {
            enterpriseDescription.text = description
        }
        
        downloadImage()
    }
    
    private func configureNavigationItem() {
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        if let title = enterpriseData?.enterprise_name {
            self.title = title
        }
    }
    
    private func downloadImage() {
        imageLoader.startAnimating()

        if let photoPath = enterpriseData?.photo {
            let url = URL(string: "http://empresas.ioasys.com.br\(photoPath)")
            enterpriseImage.kf.setImage(with: url) { result in
                self.imageLoader.stopAnimating()
                switch result {
                case .success(_):
                    break
                case .failure(_):
                    self.enterpriseImage.image = UIImage(named: "company")
                }
            }
        } else {
            self.imageLoader.stopAnimating()
            enterpriseImage.image = UIImage(named: "company")
        }

    }
}
