//
//  EnterpriseTableViewCell.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 31/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import UIKit
import Kingfisher

class EnterpriseTableViewCell: UITableViewCell {
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var imageLoader: UIActivityIndicatorView!
    @IBOutlet weak var enterpriseName: UILabel!
    @IBOutlet weak var enterpriseType: UILabel!
    @IBOutlet weak var country: UILabel!
    
    func setup(enterprise: EnterpriseData) {
        // TODO: Image download
        self.enterpriseName.text = enterprise.enterprise_name
        self.enterpriseType.text = enterprise.enterprise_type?.enterprise_type_name
        self.country.text = enterprise.country
        
        self.imageLoader.startAnimating()
        
        if let photoPath = enterprise.photo {
            let url = URL(string: "http://empresas.ioasys.com.br\(photoPath)")
            companyImage.kf.setImage(with: url) { result in
                self.imageLoader.stopAnimating()
                switch result {
                case .success(_):
                    break
                case .failure(_):
                    self.companyImage.image = UIImage(named: "company")
                }
            }
        } else {
            self.imageLoader.stopAnimating()
            companyImage.image = UIImage(named: "company")
        }
    }
}
