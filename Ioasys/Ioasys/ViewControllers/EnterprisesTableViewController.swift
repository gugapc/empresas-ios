//
//  EnterprisesTableViewController.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 29/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import UIKit

class EnterprisesTableViewController: UITableViewController {
    
    private var enterprises: Enterprises? = nil
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Pesquisar"
        searchController.searchBar.delegate = self

        UIApplication.shared.statusBarStyle = .lightContent

        addBackgroundMessage()
        configureNavigationItem()        
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprises?.enterprises.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "enterpriseCell", for: indexPath) as! EnterpriseTableViewCell
        if let enterprise = enterprises?.enterprises[indexPath.item] {
            cell.setup(enterprise: enterprise)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let enterpriseDetailStoryboard: UIStoryboard = UIStoryboard(name: "EnterpriseDetails", bundle: nil)
        let enterpriseDetailVC = enterpriseDetailStoryboard.instantiateViewController(withIdentifier: "EnterpriseDetailsID") as! EnterpriseDetailsViewController
        if let enterprise = enterprises?.enterprises[indexPath.item] {
            enterpriseDetailVC.enterpriseData = enterprise
        }

        self.navigationController?.pushViewController(enterpriseDetailVC, animated: true)
    }

    private func addBackgroundMessage() {
        let backgroundLabel = UILabel(frame: self.tableView.frame)
        backgroundLabel.text = "Clique na busca para iniciar."
        backgroundLabel.textAlignment = .center
        
        backgroundLabel.font = UIFont(name: "SFUIText-Regular", size: 18)
        backgroundLabel.textColor = UIColor(red: 56.0/255, green: 55.0/255, blue: 67.0/255, alpha: 1.0)
        
        self.tableView.backgroundView = backgroundLabel
    }
    
    private func configureNavigationItem() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 103, height: 25))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "logoHome")
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        
        self.navigationItem.titleView = imageView
        
        let searchBtn : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(self.openSearchBar))
        searchBtn.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = searchBtn;
    }
    
    @objc private func openSearchBar() {
        self.tableView.backgroundView = nil
        
        self.navigationItem.rightBarButtonItem = nil
        
        searchController.searchBar.backgroundColor = self.navigationController?.navigationBar.barTintColor
        self.navigationItem.titleView = searchController.searchBar
        self.navigationItem.titleView?.contentMode = .center
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        configureNavigationItem()
    }
}

extension EnterprisesTableViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    }
}

extension EnterprisesTableViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBarText = searchController.searchBar.text ?? ""
//        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
//        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
        
        NetworkRequestAPI.searchWith(name: searchBarText) { (enterprises, error, cache) in
            if let enterprises = enterprises {
                print("==========>>>>>>\(enterprises)")
                self.enterprises = enterprises
                self.tableView.reloadData()
            }
            // TODO: error handler
        }
    }
}
