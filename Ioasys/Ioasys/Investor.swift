//
//  Investor.swift
//  Ioasys
//
//  Created by Gustavo Pereira on 30/08/19.
//  Copyright © 2019 Gustavo Pereira. All rights reserved.
//

import Foundation

struct Investor: Mappable {
    //    var photo:
    //    var portfolio:
    var id: Int
    var investor_name: String
    var email: String
    var city: String
    var country: String
    var balance: Double
    var portfolio_value: Double
    var first_access: Bool
    var super_angel: Bool
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.investor_name = mapper.keyPath("investor_name")
        self.email = mapper.keyPath("email")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.balance = mapper.keyPath("balance")
        self.portfolio_value = mapper.keyPath("portfolio_value")
        self.first_access = mapper.keyPath("first_access")
        self.super_angel = mapper.keyPath("super_angel")
    }
}
